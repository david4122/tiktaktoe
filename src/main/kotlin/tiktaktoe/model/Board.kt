package tiktaktoe.model

import javafx.beans.property.SimpleObjectProperty

class FieldTakenException: Exception()

enum class Player(val label: String) {
	X("X"), O("O"), EMPTY(" ")
}

class Board {

	private val board = Array(3) { Array(3) { Player.EMPTY } }

	val onMark = mutableListOf<(Int, Int, Player) -> Unit>()
	var turn = SimpleObjectProperty<Player>(Player.X)
	val size: Int
		get() = board.size

	operator fun get(x: Int, y: Int) = board[x][y]
	operator fun set(x: Int, y: Int, p: Player) { board[x][y] = p }

	fun clear() {
		(0..8).forEach { board[it / 3][it % 3] = Player.EMPTY }
	}

	fun mark(x: Int, y: Int): Player {
		if(board[x][y] != Player.EMPTY)
			throw FieldTakenException()
		board[x][y] = turn.get()
		onMark.forEach { it(x, y, turn.get()) }

		val checks = mutableListOf<(Int) -> Player>({ board[x][it] },
				{ board[it][y] })

		if(x == y)
			checks.add { board[it][it] }

		if((x + y) % 2 == 0)
			checks.add { board[it][2 - it] }

		val res = if(checks.map(::check).filter { it == true }.any()) board[x][y]
					else Player.EMPTY
		turn.set(turn.get().next())
		return res
	}

	private fun check(f: (Int) -> Player): Boolean {
		for(i in 0..2) {
			if(f(i) != turn.get())
				return false
		}
		return true
	}

	fun Player.next(): Player = when(this) {
		Player.X -> Player.O
		Player.O -> Player.X
		Player.EMPTY -> Player.EMPTY
	}
}
