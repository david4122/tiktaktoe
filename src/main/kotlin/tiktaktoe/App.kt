package tiktaktoe

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.Button
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.stage.Stage
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.beans.binding.Bindings
import java.util.concurrent.Callable
import javafx.scene.control.Alert
import javafx.scene.control.Alert.AlertType
import java.util.Arrays

import tiktaktoe.model.Board
import tiktaktoe.model.Player
import tiktaktoe.model.FieldTakenException

class App: Application() {

	@FXML lateinit var boardView: GridPane
	@FXML lateinit var turnView: Label

	lateinit var stage: Stage
	private val board = Board()

	override fun start(stg: Stage) {
		stage = stg

		val loader = FXMLLoader(App::class.java.getResource("/layout.fxml"))
		loader.setController(this)
		val s = Scene(loader.load())

		stage.setScene(s)
		stage.setTitle("TikTakToe")
		stage.show()
	}

	@FXML fun initialize() {
		board.onMark.add { x, y, p ->
			(boardView.getChildren().get(y * 3 + x) as Button).setText(p.label)
		}

		boardView.getChildren().clear()
		buildBoard()
		turnView.textProperty().bind(Bindings.createStringBinding(
			object: Callable<String> {
				override fun call(): String =
					"Current turn: " + board.turn.get().label
			}, board.turn))
	}

	@FXML fun restartGame() {
		board.clear()
		for(i in 0..(board.size - 1)) {
			for(j in 0..(board.size - 1))
				boardView[i, j].setText(board[i, j].label);
		}
	}

	@FXML fun quit() = stage.close()

	private fun buildBoard() {
		for(i in 0..2) {
			for(j in 0..2) {
				boardView.add(buildButton(), i, j)
			}
		}
	}

	private fun buildButton(): Button {
		return Button().apply {
			setMaxWidth(Double.MAX_VALUE)
			setMaxHeight(Double.MAX_VALUE)

			setOnAction { mark() }

			GridPane.setHgrow(this, Priority.ALWAYS)
			GridPane.setVgrow(this, Priority.ALWAYS)
		}
	}

	fun alert(t: AlertType, s: String) = Alert(t).apply {
			setHeaderText(s)
		}.showAndWait()


	operator fun GridPane.get(x: Int, y: Int): Button =
			getChildren().get(y * board.size + x) as Button

	val Button.x: Int
		get() = GridPane.getRowIndex(this)

	val Button.y: Int
		get() = GridPane.getColumnIndex(this);

	fun Button.mark() {
		try {
			val won = board.mark(x, y);
			if(won != Player.EMPTY) {
				alert(AlertType.INFORMATION, "${won} won!")
				restartGame()
			}
		} catch(e: FieldTakenException) {
			alert(AlertType.ERROR, "This field is taken")
		}
	}
}

fun main(args: Array<String>) {
	Application.launch(App::class.java, *args)
}
